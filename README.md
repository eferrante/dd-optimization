# DD-Optimization

DD-Optimization library was written by Enzo Ferrante ( ferrante.enzo@gmail.com ). It is distributed under the GNU 3.0 License terms.
It implements the Dual Decomposition framework for discrete optimization (**[1]**).

# Instructions to compile the toy example in Ubuntu

 * Unpack the code (e.g. in the dir /home/x/toy_dd/)
 * In Ubuntu, install 'cmake' and 'qt5-default' packages
 * Create a 'build' dir in the source code dir: /home/x/toy_dd/build/
 ```
 $ cd /home/x/toy_dd/build/
 $ cmake ..
 $ make
 ```
 
 * Check if 'ipImg.txt' is in the correct location /home/x/toy_dd/data/
 * Now you can run the sample code: 
 ```$ ./ToyDD ```

# Citation

This code was initially written to optimize a discrete higher order probabilistic graphical model to perform slice-to-volume registration. 
Please, if you use the code for your reserch, cite our patent [*Paragios, Nikos, Enzo Ferrante, and Rafael Marini Silva.* **Method and device for elastic registration between a two-dimensional digital image and a slice of a three-dimensional volume with overlapping content.** U.S. Patent No. 9,418,468. 16 Aug. 2016.](https://www.google.com/patents/US9418468)

# Acknowleadments

Thanks to Hariprasad Kannan, Evgenios Kornaropoulos and Vivien Fecamp for their comments and contributions to improve this code.

## References

**[1]** Komodakis N, Paragios N, and Tziritas G. "MRF energy minimization and beyond via dual decomposition." IEEE transactions on pattern analysis and machine intelligence 33.3 (2011): 531-552.
