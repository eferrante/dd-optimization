#include <QCoreApplication>
#include <cstdio>
#include <iostream>

#include "DDGraph.h"
#include "LabelTableSimple.h"
#include "PartitionerSquareCliques.h"
#include "SlaveOptimizerExhaustiveSearch.h"
#include "Clique.h"
#include "Slave.h"
#include "DDCostFunctionO4L1Denoise.h"
#include "DualDecomposition.h"

using namespace std;
using namespace DD;

int main(int argc, char *argv[])
{

    cout << "Initializing DD structures..."<<endl;
    SlaveOptimizerExhaustiveSearch * bruteForceOptimizer = new SlaveOptimizerExhaustiveSearch();

    LabelTableSimple * labelTable = new LabelTableSimple(10);

    int dimX = 30;
    int dimY = 30;

    float* ipImg = new float[dimX*dimY];

    FILE* fIpImg = fopen("../data/ipImg.txt","r");

    for (int i = 0; i != dimX*dimY; ++i)
    {
        fscanf(fIpImg,"%f",&ipImg[i]);
    }

    fclose(fIpImg);

    cout<< "Read the image"<<endl;

    DDCostFunctionO4L1Denoise * costFunction= new DDCostFunctionO4L1Denoise(labelTable, ipImg, dimX, dimY, 0.1);

    DDGraph * graph = new DDGraph(dimX,dimY,1);

    PartitionerSquareCliques * partitionerSquare = new PartitionerSquareCliques( bruteForceOptimizer, costFunction );

    DualDecomposition * dd = new DualDecomposition(graph, labelTable, 1000, 1.0);

    dd->addPartitioner(partitionerSquare);

    dd->applyPartitioners(); 

    cout<< "Applied partitioner"<<endl;

    costFunction->precalculateCosts(dd);

    cout << "Done..."<<endl;

    dd->resetSlavesPrices();

    cout<<"prices reset"<<endl;

    double energy = dd->run();

    cout << "Final Energy = " << energy;

    return 0;

}


