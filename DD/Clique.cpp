#include "Clique.h"

namespace DD
{

	Clique::Clique (int id, int order, int * nodes, DDCostFunction * costFunction) : id(id), order(order), nodes(nodes), costFunction(costFunction)
	{
		
	}

	float Clique::evaluate(int * labelAssignement) 
	{	 
		return costFunction->evaluate(this, labelAssignement); 
	}

	Clique::~Clique()
	{
		delete[] nodes;
	};
}