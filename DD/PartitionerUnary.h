#ifndef PARTITIONERUNARY_DD_H
#define PARTITIONERUNARY_DD_H

#include "DDGraph.h"
#include "Slave.h"
#include "Clique.h"
#include "DDGraph2D.h"
#include "Partitioner.h"
#include "SlaveOptimizer.h"
#include "DDCostFunction.h"
#include "LabelTable.h"

#include <QList>

namespace DD
{
	/**
     * \brief PartitionerUnary class. It creates a
	 * 
	 * \author Enzo Ferrante.
	 * \date 30-11-2012
	 *
	 */
	
	class PartitionerUnary : public Partitioner
	{
		public:
			/**
			* Unary paritioner.
			* \param node Node in which the slave and clique are going to be created.
			*/
			PartitionerUnary(SlaveOptimizer * optimizer, DDCostFunction * costFunction, int node) : 
			  Partitioner(optimizer, costFunction), node(node) {};

			/**
			* This method partitiones the graph.
			*/
			void partition(DDGraph * graph, LabelTable * labelTable, QList<Slave*> * slaves, QList<Clique*> * cliques)
			{
					// Create the new slave
                    Slave * s = new Slave(slaves->size(), optimizer, graph->getNodesCount(), 1, 1, labelTable);

					s->addNode(node);

					// Create the new clique
					int * cliqueNodes = new int[1];
					cliqueNodes[0] = node;

					Clique * c = new Clique(cliques->size(), 1, cliqueNodes, costFunction);

					// Add the new clique to the slave and to the cliques list
					s->addClique(c);
					cliques->append(c);
				
					// Add the new slave to the node in the graph 
					g->addSlave(node, s);

					// Add the new slave to the slaves list
					slaves->append(s);
		}		

		private:

			int node;		///> Node in which the slave and clique are going to be created.
    };
};

#endif
