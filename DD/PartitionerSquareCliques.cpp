#include "PartitionerSquareCliques.h"
#define GET_INDEX(x, y, width) (x) + (y) * width

namespace DD {

void PartitionerSquareCliques::partition(DDGraph * graph, LabelTable * labelTable, QList<Slave*> * slaves, QList<Clique*> * cliques)
{    
    int dimX = graph->getCols(); int dimY = graph->getRows();

    // Calculate the amount of slaves and cliques per slave
    int slavesAmount = (dimX - 1) * (dimY - 1);
    int nodesPerSlave = 4;
    int cliquesPerSlave = 1;
    int totalGraphNodes = graph->getNodesCount();
    int labelsAmount = labelTable->getLabelsCount();

    Slave * s;
    Clique * c;
    int * cliqueNodes;

    // For each column
    for (int y = 0; y <	dimY - 1; ++y)
    {
        for (int x = 0; x < dimX - 1; ++x)
        {
            // ======== SQUARE =======

            // Create the new slave
            s = new Slave(slaves->size(), optimizer, totalGraphNodes, nodesPerSlave, cliquesPerSlave, labelTable);

            // Add the corresponding nodes to the slave
            s->addNode(GET_INDEX(x, y, dimX));
            s->addNode(GET_INDEX(x, y+1, dimX));
            s->addNode(GET_INDEX(x+1, y, dimX));
            s->addNode(GET_INDEX(x+1, y+1, dimX));

            // Create the new clique
            cliqueNodes = new int[4];
            cliqueNodes[0] = GET_INDEX(x, y, dimX);
            cliqueNodes[1] = GET_INDEX(x, y+1, dimX);
            cliqueNodes[2] = GET_INDEX(x+1, y, dimX);
            cliqueNodes[3] = GET_INDEX(x+1, y+1, dimX);

            c = new Clique(cliques->size(), 4, cliqueNodes, costFunction);

            // Add the new clique to the slave and to the cliques list
            s->addClique(c);
            cliques->append(c);

            // Add the new slave to the nodes in the graph
            graph->addSlave(cliqueNodes[0], s);
            graph->addSlave(cliqueNodes[1], s);
            graph->addSlave(cliqueNodes[2], s);
            graph->addSlave(cliqueNodes[3], s);

            // TODO: Is it necessary to add the clique to the node? In that case it should be done here.

            // Add the new slave to the slaves list
            slaves->append(s);
        }
    }
}

}
