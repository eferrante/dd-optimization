#ifndef PARTITIONER_DD_H
#define PARTITIONER_DD_H

#include "DDGraph.h"
#include "Slave.h"
#include "Clique.h"
#include "SlaveOptimizer.h"
#include "DDCostFunction.h"
#include "LabelTable.h"
#include <QList>

namespace DD
{
	/**
	 * \brief (Abstract class) Partitioner class. It partitiones a given graph in slaves and cliques, and add them to the given lists.
	 * 
	 * 
	 * \author Enzo Ferrante.
	 * \date 28-11-2012
	 *
	 */
	
	class Partitioner
	{
		public:				
			Partitioner(SlaveOptimizer * optimizer, DDCostFunction * costFunction) :
			  optimizer(optimizer), costFunction(costFunction) {};

			/**
			* This method partitiones the graph in different slaves and cliques, and assignes the given optimizer to every slave and the given cost function to every clique.
			*/
			virtual void partition(DDGraph * graph, LabelTable * labelTable, QList<Slave*> * slaves, QList<Clique*> * cliques) = 0;

		protected:
			SlaveOptimizer * optimizer;
			DDCostFunction * costFunction;
	};
};

#endif