#include "DualDecompositionFast.h"

using namespace std;

namespace DD
{
	//==========================================================================================================

	DualDecompositionFast::DualDecompositionFast(DDGraph * graph, LabelTable * labelTable, int maxIterations, float alpha) :
		graph(graph), labelTable(labelTable), maxIterations(maxIterations), slaves(0), cliques(0), solutionLabeling(0), alpha(alpha), initialPrices(0)
	{

	}

	DualDecompositionFast::~DualDecompositionFast()
	{
		for (int i = 0; i < slaves->size(); ++i)
			delete slaves->at(i);
		delete slaves;
		
		for (int i = 0; i < cliques->size(); ++i)
			delete cliques->at(i);
		delete cliques;
		
		delete[] solutionLabeling;
	}

	//==========================================================================================================

	void DualDecompositionFast::applyPartitioners()
	{
		// Delete every existing slave
		if (slaves)
			for (int i = 0, size = slaves->size(); i < size; ++i)
				delete slaves->at(i);
		
		// Delete every existing clique
		if (cliques)
			for (int i = 0, size = slaves->size(); i < size; ++i)
				delete slaves->at(i);

		// Clean the graph cliques
		graph->eraseSlavesInformation();

		slaves = new QList<Slave *>;
		cliques = new QList<Clique *>;

		// Partition the graph in cliques and slaves using the partitioners
		for (int i = 0; i < partitioners.size(); ++i)
			partitioners.at(i)->partition(graph, labelTable, slaves, cliques);

	}
	
	//==========================================================================================================
	float DualDecompositionFast::getTotalEnergy()
	{
		return getTotalEnergy(this->solutionLabeling);
	}

	float DualDecompositionFast::evaluateTotalCostSumZeroLabeling()
	{
		int * labeling = new int[graph->getNodesCount()];
		std::fill(labeling, labeling+graph->getNodesCount(), 0);

		float energy = getTotalEnergy(labeling);

		delete [] labeling;

		return energy;
	};

	float DualDecompositionFast::getTotalEnergy(int * labeling)
	{
		float energy = 0.0f;

		if (cliques)
			for (int i = 0; i < cliques->size(); i++)
				energy += cliques->at(i)->evaluate( labeling );
		
		return energy;
	}

	//==========================================================================================================

	void DualDecompositionFast::reconstructFinalSolution() 
	{
		if (solutionLabeling)
			delete solutionLabeling;

		solutionLabeling = new int[graph->getNodesCount()];

		int * labelAssignments;

		if (slaves)
			for (int i = 0; i < slaves->size(); ++i)
			{
				labelAssignments = slaves->at(i)->getNodesLabelAssignements();

				for (int j = 0; j < graph->getNodesCount(); ++j)
					if (labelAssignments[j] != INVALID_LABEL)
						solutionLabeling[j] = labelAssignments[j];
			}

	}


	//==========================================================================================================

	float DualDecompositionFast::run()
	{
		int it = 1;
		
		if (graph && slaves && cliques)
		{
			bool convergence = false;
		
			// write info file
			//std::ofstream dualEnergyFile;	

			//dualEnergyFile.open( "dualEnergy.csv" );
			cout << "Alpha: " << alpha << endl;
	
			float factor = alpha;
			
			while (!convergence && (it < maxIterations))
			{			
				//std::cout << "Iteration " << it;
				
				float dualEnergy = 0;

				int size = slaves->size();
				// Optimize every slave
				#pragma omp parallel for
				for (int i = 0; i < size; ++i)
				{
					dualEnergy += slaves->at(i)->optimize();;
					//slaves->at(i)->showLabelAssignements();
					//std::cout << "Energy: " << e << std::endl;
					//slaves->at(i)->showPrices();
				}

				// Update the prices of the slaves and check the convergence of the algorithm at the same time
				convergence = updatePrices(it, factor);
				//std::cout << "   Dual Energy: " << dualEnergy << std::endl;
				//dualEnergyFile << it << ", " << dualEnergy << std::endl;
						
				// If there are some initial prices, they are summed to the primal energy cause they are considered as Unary Potentials.
				
				if ((it %200) == 0) {
				std::cout << "   It: " << it << std::endl;
				std::cout << "   Dual Energy: " << dualEnergy << std::endl;
				}

				it++;
			}

			if (convergence)
				cout << "Convergence: True in " << it << " iterations" << endl;
			else
				cout << "Convergence: False" << endl;

			
			reconstructFinalSolution();

			//dualEnergyFile.close();

			return getTotalEnergy();
		}
		else
		{
			std::cout << "DualDecompositionFast ERROR: Not graph or slaves or cliques defined";
			return -1;
		}
	}

	//==========================================================================================================

	bool DualDecompositionFast::updatePrices(int it, float energyGap)
	{
		int nodesCount = graph->getNodesCount();

		int slavesCount = 0;
		float factor;

		Slave ** nodeSlaves;
		int notAgreeing = 0;
		int nodesNotAgreeing=0;
		int labelsCount = labelTable->getLabelsCount();

		int * labelsCounting = new int[labelsCount];
		
		bool converge = true;

		// For each node n
		for (int n = 0; n < nodesCount; ++n)
		{
			bool agreeing = true;

			std::fill(labelsCounting, labelsCounting + labelsCount, 0);

			// For each pair of slaves s1, s2 where the node n is used
			graph->getSlaves(n, nodeSlaves, slavesCount);
			
			//std::cout << "Alpha " << (alpha / it) / slavesCount << endl;
			int l1, l2;
					
			for (int s1 = 0; s1 < slavesCount; ++s1)
				for (int s2 = s1 + 1; s2 < slavesCount; ++s2)
				{
					// Get the label that corresponds to node p in each slave
					l1 = (nodeSlaves[s1]->getNodesLabelAssignements())[n];
					l2 = (nodeSlaves[s2]->getNodesLabelAssignements())[n];

					// If the assigned label is not the same
					if ( l1 != l2 ) 
					{
						// Update the prices for the labels l1, l2 in the slaves s1, s2
						//factor = (alpha / it);// / slavesCount;
						//if (it < 200)
						factor = (alpha / (( (static_cast<float>(it)) * static_cast<float>(slavesCount))));
						
						//factor = alpha;
						nodeSlaves[s1]->updatePrice(n, l1, factor);
						nodeSlaves[s1]->updatePrice(n, l2, -factor);
						nodeSlaves[s2]->updatePrice(n, l1, -factor);
						nodeSlaves[s2]->updatePrice(n, l2, factor);
						converge = false;
						notAgreeing++;
						agreeing = false;
					}	
				}
			
			if(!agreeing) 
			{
				nodesNotAgreeing++;
			}
		}

		if (it % 200 == 0)
		{
			cout << "Pairs not agreeing: " << notAgreeing << endl;
			cout << "Nodes not agreeing: " << nodesNotAgreeing << endl;
		}	

		return converge;
	}

	void DualDecompositionFast::show() 
	{
		graph->show();
	
		if (slaves)
		{
			cout << "[ == Slaves == ]" << endl;
			for (int i = 0; i < slaves->size(); ++i)
			{
				Slave * slave = slaves->at(i);
				cout << "|- Slave " << slave->getId() << endl;
				cout << "  |- Nodes" << endl;

				for (int j = 0; j < slave->getNodesSlaveAmount(); ++j)
				{
					cout << "    |- " << slave->getNodes()[j] << endl;
				}
				
				cout << "  |- Cliques" << endl;

				for (int j = 0; j < slave->getCliquesSlaveAmount(); ++j)
				{
					Clique * clique = slave->getCliques()[j];
					cout << "    |- (";
					for (int k = 0; k < clique->getOrder(); ++k)
						cout << clique->getNodes()[k] << " ";
					cout << ")" << endl;
				}
				cin.get();			
			}
		}
	}

	void DualDecompositionFast::initializePrices(float * prices)
	{
		int size = slaves->size();
		
		initialPrices = prices;

		#pragma omp parallel for
		for (int i = 0; i < size; ++i)
			slaves->at(i)->initializePrices(prices);
			
	}

	void DualDecompositionFast::resetSlavesPrices() 
	{
		int size = slaves->size();

		#pragma omp parallel for
		for (int i = 0; i < size; ++i)
			slaves->at(i)->resetPrices();
	}

}