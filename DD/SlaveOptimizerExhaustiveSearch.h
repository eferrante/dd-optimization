#ifndef SLAVEOPTIMIZEREXHAUSTIVESEARCH_DD_H
#define SLAVEOPTIMIZEREXHAUSTIVESEARCH_DD_H
#include "SlaveOptimizer.h"
#include <limits>

namespace DD
{
	/**
	 * Slave Optimization by exhaustive search.
	 * 
	 * \author Vivien F�camp.
	 * \date 28-11-2012
	 *
	 */

	class SlaveOptimizerExhaustiveSearch : public SlaveOptimizer
	{

		public:

			SlaveOptimizerExhaustiveSearch();

			/**
			* Optimize the given slave and returns the obtained energy value.
			*/
			float optimize(Slave * slave);
			
		private:
			long int ipow(int base, int exp);
	
	};
};

#endif