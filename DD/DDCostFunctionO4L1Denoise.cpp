#include "DDCostFunctionO4L1Denoise.h"
#include "DualDecomposition.h"

#define GET_LABEL(x) labelAssignment[ clique->getNodes()[x] ]
#define GET_INDEX(x, y, width) (x) + (y) * width

namespace DD {

    DDCostFunctionO4L1Denoise::DDCostFunctionO4L1Denoise(LabelTableSimple * labelTable, float* ipImg, int dimX, int dimY, float factor) : DDCostFunction(factor), m_labelTable(labelTable), m_ipImg(ipImg), m_dimX(dimX), m_dimY(dimY)
    {
        // Possible number of labels in the labelspace
        m_l = m_labelTable->getLabelsCount();
        m_l_quadruple = m_l * m_l * m_l * m_l;
        m_l_cube = m_l * m_l * m_l;
        m_l_squared = m_l * m_l;
    }


    float DDCostFunctionO4L1Denoise::evaluate(Clique * clique, int * labelAssignment)
    {
        return m_costs[ clique->getId() * m_l_quadruple + GET_LABEL(0) * m_l_cube + GET_LABEL(1) * m_l_squared + GET_LABEL(2) * m_l + GET_LABEL(3)];
    }


    void DDCostFunctionO4L1Denoise::precalculateCosts(DualDecomposition* dd)
    {
        QList<Clique *> *cliques = dd->getCliques();
        DDGraph* graph = dd->getDDGraph();
        m_numCliques = cliques->size();

        m_costs = new float[m_numCliques * m_l_quadruple];

        for (int clique = 0; clique < m_numCliques; clique++)
            for (int l0 = 0; l0 < m_l; ++l0)
                for (int l1 = 0; l1 < m_l; ++l1)
                    for (int l2 = 0; l2 < m_l; ++l2)
                        for (int l3 = 0; l3 < m_l; ++l3)
                         {
                            int * curNodes = (*cliques)[clique]->getNodes();
                            float curCost = 0;
                            Slave** slaves;
                            int slavesCount;

                            graph->getSlaves(curNodes[0], slaves, slavesCount);
                            curCost += ((m_ipImg[curNodes[0]] - l0)*(m_ipImg[curNodes[0]] - l0))/slavesCount;
                            graph->getSlaves(curNodes[1], slaves, slavesCount);
                            curCost += ((m_ipImg[curNodes[1]] - l1)*(m_ipImg[curNodes[1]] - l1))/slavesCount;
                            graph->getSlaves(curNodes[2], slaves, slavesCount);
                            curCost += ((m_ipImg[curNodes[2]] - l2)*(m_ipImg[curNodes[2]] - l2))/slavesCount;
                            graph->getSlaves(curNodes[3], slaves, slavesCount);
                            curCost += ((m_ipImg[curNodes[3]] - l3)*(m_ipImg[curNodes[3]] - l3))/slavesCount;

                            float avgLab = (l0 + l1 + l2 + l3)/4.0;

                            curCost += ((l0 - avgLab)*(l0 - avgLab) + (l1 - avgLab)*(l1 - avgLab) + (l2 - avgLab)*(l2 - avgLab) + (l3 - avgLab)*(l3 - avgLab))/4;

                            m_costs[ clique * m_l_quadruple + l0 * m_l_cube + l1 * m_l_squared + l2 * m_l + l3] = factor*curCost;
                         }
    }
}
