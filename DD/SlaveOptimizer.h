#ifndef SLAVEOPTIMIZER_DD_H
#define SLAVEOPTIMIZER_DD_H

#include "Slave.h"
#include "Clique.h"

namespace DD
{
	/**
	 * \brief (Abstract class) SlaveOptimizer class. It optimizes a Slave storing on it the resulting label set and returning the energy value.
	 * 
	 * 
	 * \author Enzo Ferrante.
	 * \date 28-11-2012
	 *
	 */
	class Slave;

	class SlaveOptimizer
	{

		public:
			/**
			* Optimize the given slave and returns the obtained energy value.
			*/
			virtual float optimize(Slave * slave) = 0;

	};
};

#endif