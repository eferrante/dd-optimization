#include "Slave.h"

using namespace std;

namespace DD {
	//==========================================================================================================

	Slave::Slave(int id, SlaveOptimizer * optimizer, int nodesTotalAmount, int nodesApproxSlaveAmount, int cliquesApproxSlaveAmount, LabelTable * labelTable) : 
		id(id), optimizer(optimizer), nodesTotalAmount(nodesTotalAmount), nodesApproxSlaveAmount(nodesApproxSlaveAmount), cliquesApproxSlaveAmount(cliquesApproxSlaveAmount), labelTable(labelTable)
	{	
		this->nodesLabelAssignements = new int[nodesTotalAmount];
		this->localNodeId = new int[nodesTotalAmount];

		// Create the vectors to store nodes and cliques information and reserves memory for them using the given approx amounts.
		nodes = new QVector<int>;
		nodes->reserve(nodesApproxSlaveAmount);
		
		// Create the vector to store the price associated to every node for every label
		nodesPrices = new QVector<float*>;
		nodesPrices->reserve(nodesApproxSlaveAmount);
		
		cliques = new QVector<Clique *>;
		cliques->reserve(cliquesApproxSlaveAmount);

		// Fill the array of label assignements with a default value 
		fill(this->nodesLabelAssignements, this->nodesLabelAssignements + nodesTotalAmount, INVALID_LABEL);
		// Fill the array of local node ids with a invalid default value 
		fill(this->localNodeId, this->localNodeId + nodesTotalAmount, INVALID_LABEL);

	}

	Slave::~Slave()
	{
		delete[] nodesLabelAssignements;
		delete[] localNodeId;
		delete nodes;
		delete cliques;
		
		for (int i = 0; i < nodesPrices->size(); ++i)
			delete[] nodesPrices->at(i);		
	}
	//==========================================================================================================

	float Slave::optimize()
	{
		return optimizer->optimize(this);
	}

	//==========================================================================================================

	void Slave::updatePrice(int node, int label, float factor)
	{
		nodesPrices->at(localNodeId[node])[label] += factor; 
	}

	void Slave::initializePrices(float * prices)
	{
		int slaveNodes = nodes->size();
		int labelsAmount = labelTable->getLabelsCount();

		for (int i=0; i < slaveNodes; ++i)
			for(int label = 0; label < labelsAmount; ++label)
				nodesPrices->at(i)[label] = prices[nodes->at(i) + nodesTotalAmount * label]; 
	}

	void Slave::resetPrices()
	{
		int slaveNodes = nodes->size();
		int labelsAmount = labelTable->getLabelsCount();

		for (int i = 0; i < slaveNodes; ++i)
			for(int label = 0; label < labelsAmount; ++label)
				nodesPrices->at(i)[label] = 0;

	}


}