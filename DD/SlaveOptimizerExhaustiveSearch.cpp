#include "SlaveOptimizerExhaustiveSearch.h"

namespace DD 
{
	long int SlaveOptimizerExhaustiveSearch::ipow(int base, int exp)
	{
		long int result = 1;
		while (exp)
		{
			if (exp & 1)
				result *= base;
			exp >>= 1;
			base *= base;
		}

		return result;
	}

	SlaveOptimizerExhaustiveSearch::SlaveOptimizerExhaustiveSearch()
	{

	}

	float SlaveOptimizerExhaustiveSearch::optimize(Slave * slave)
	{
		// Number of labels in the table
		int nb_labels = slave->getLabelTable()->getLabelsCount();
		
		// Array of nodes in the slave
		int * nodes = slave->getNodes(); 
		
		// Number of nodes in the slave
		int nb_nodes = slave->getNodesSlaveAmount();
		
		// Number of nodes in the graph
		int totalNodesGraph = slave->getNodesTotalAmount();

		// Array of cliques in the slave
		Clique ** cliques = slave->getCliques(); 

		// Number of cliques in the slave
		int nb_cliques = slave->getCliquesSlaveAmount();

		// Label set. This array is used to generate the temporary assignment of labels to the nodes during the optimization process.
		int * label_set; label_set = new int[totalNodesGraph];
		
		// Label set clique. This array is used to pass a label set to the clique. Its size will be equal to the order of the clique.
		//int * label_set_clique; 

		// Amount of possible label sets.
		int maxi = ipow(nb_labels, nb_nodes);

		long int temp = 0; int count = 0;

		float current_cost = 0.0f; float best_cost = std::numeric_limits<float>::max();
		
		// Get the reference to the array where the solution will be stored
		int * solution = slave->getNodesLabelAssignements();

		// Aux variable to store the bestSolution index
		int bestSolution;
		int s = slave->getId();

		//std::cout << "Optimizing slave " << slave->getId() << std::endl;

		// Main loop, over all possibilities
		for (long int alllabel = 0; alllabel < maxi; ++alllabel)
		{
			// Decode the label assignment for each node in the slave, corresponding to the index "alllabel" and stores it into the array label_set.
			temp = alllabel;	
			current_cost = 0;

			for (int k=0; k < nb_nodes; ++k)
			{
				label_set[ nodes[nb_nodes-k-1] ] = temp % nb_labels;
				temp = (temp - label_set[ nodes[nb_nodes-k-1] ]) / nb_labels;
			}
			 
			// For each clique, evaluate the cost and sum up
			for (int nbc = 0; nbc < nb_cliques; ++nbc)
			{
				// Add the cost that corresponds to the clique
				current_cost += cliques[nbc]->evaluate(label_set);
			}

			// Add the prices payed for choosing the labels
			current_cost += slave->calculateLabelPrices(label_set);

			// Check if we have the best combination, up to now
			if (current_cost < best_cost)
			{
				best_cost = current_cost;
				bestSolution = alllabel;
			}
		}

		// Decode the best solution
		for (int k=0; k < nb_nodes; ++k)
		{
			solution[ nodes[nb_nodes-k-1] ] = bestSolution % nb_labels;
			bestSolution = (bestSolution - solution[ nodes[nb_nodes-k-1] ]) / nb_labels;
		}

		delete [] label_set;

		return best_cost;
	}
}

//	float SlaveOptimizerExhaustiveSearch::optimize(Slave * slave)
//	{
//		// Number of labels in the table
//		int nb_labels = slave->getLabelTable()->getLabelsCount();
//		
//		// Array of nodes in the slave
//		int * nodes = slave->getNodes(); 
//		
//		// Number of nodes in the slave
//		int nb_nodes = slave->getNodesSlaveAmount();
//		
//		// Number of nodes in the graph
//		int totalNodesGraph = slave->getNodesTotalAmount();
//
//		// Array of cliques in the slave
//		Clique ** cliques = slave->getCliques(); 
//
//		// Number of cliques in the slave
//		int nb_cliques = slave->getCliquesSlaveAmount();
//
//		// Label set. This array is used to generate the temporary assignment of labels to the nodes during the optimization process.
//		int * label_set; label_set = new int[totalNodesGraph];
//		
//		// Label set clique. This array is used to pass a label set to the clique. Its size will be equal to the order of the clique.
//		int * label_set_clique; 
//
//		// Amount of possible label sets.
//		int maxi = ipow(nb_labels, nb_nodes);
//
//		long int temp = 0; int count = 0;
//
//		float current_cost = 0.0f; float best_cost = std::numeric_limits<float>::max();
//		
//		Clique * A;
//
//		// Get the reference to the array where the solution will be stored
//		int * solution = slave->getNodesLabelAssignements();
//
//		// Aux variable to store the bestSolution index
//		int bestSolution;
//
//		// Main loop, over all possibilities
//		for (long int alllabel = 0; alllabel < maxi; ++alllabel)
//		{
//			// Decode the label assignment for each node in the slave, corresponding to the index "alllabel" and stores it into the array label_set.
//			temp = alllabel;
//			current_cost = 0;
//
//			for (int k=0; k < nb_nodes; ++k)
//			{
//				label_set[ nodes[nb_nodes-k-1] ] = temp % nb_labels;
//				temp = (temp - label_set[ nodes[nb_nodes-k-1] ]) / nb_labels;
//			}
//			 
//			// For each clique, evaluate the cost and sum up
//			for (int nbc = 0; nbc < nb_cliques; ++nbc)
//			{
//				// Get the clique
//				A = cliques[nbc];
//
//				// Get the nodes of the clique
//				int * nodes_clique = A->getNodes();
//
//				// Set of labels for the clique
//				label_set_clique = new int [A->getOrder()];
//
//				// Copy the corresponding label value assigned to the nodes in the clique
//				for (int i = 0; i < A->getOrder(); ++i)
//					label_set_clique[i] = label_set[ nodes_clique[i] ];
//
//				current_cost += A->evaluate(label_set_clique);
//
//				delete label_set_clique;
//			}
//
//			// Check if we have the best combination, up to now
//			if (current_cost < best_cost)
//			{
//				best_cost = current_cost;
//				bestSolution = alllabel;
//			}
//		}
//
//		// Decode the best solution
//		for (int k=0; k < nb_nodes; ++k)
//		{
//			solution[ nodes[nb_nodes-k-1] ] = bestSolution % nb_labels;
//			bestSolution = (bestSolution - label_set[ nodes[nb_nodes-k-1] ]) / nb_labels;
//		}
//
//		delete label_set;
//	}
//}