#ifndef CLIQUE_DD_H
#define CLIQUE_DD_H

#include "Slave.h"
#include "DDCostFunction.h"

#include <QList>

namespace DD
{
	class Slave;
	class DDCostFunction;

	/**
	 * \brief Clique class. It models a clique of a graph and it's used by Dual Decomposition algorithm (DD Class).
	 * 
	 * \author Enzo Ferrante.
	 * \date 28-11-2012
	 *
	 */
	class Clique
	{	
		public:
			/**
			* Clique constructor
			*/
			Clique (int id, int order, int * nodes, DDCostFunction * costFunction);
			
			~Clique();

			/**
			* Add a slave to the clique's slave list. It indicates that this clique is used in this slave.
			*/
			void addSlave(Slave * s) { slaves.append(s); }

			/**
			* Evaluate this clique with the given labels using the DDCostFunction associated.
			* \param labels array containing the label assignement for all the nodes in the graph. The nodes in the clique must have a valid label value.
			*/
			float evaluate(int * labelAssignement);

			/**
			* Getters
			*/
			int getId() { return id; }
			int getOrder() { return order; }
			int * getNodes() { return nodes; }

	private:
			
			int id;					///< Clique ID. This ID must be unique for a clique in a graph.
			int order;				///< Indiquetes the order (size) of the clique.			
			int * nodes;			///< The nodes included in the clique. The size of this vector is 'order'.

			QList<Slave*> slaves;	///< List of slaves where this Clique is used.

			DDCostFunction * costFunction;	///< Cost function used to evaluate the energy of the clique.
	};
};
#endif