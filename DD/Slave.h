#ifndef SLAVE_DD_H
#define SLAVE_DD_H

#include "SlaveOptimizer.h"
#include "Clique.h"
#include "LabelTable.h"
#include <iostream>
#include <QVector>

namespace DD
{
	/**
	 * \brief Slave class. It models a slave used by the Dual Decomposition algorithm.
	 * 
	 * 
	 * \author Enzo Ferrante.
	 * \date 28-11-2012
	 *
	 */

	class SlaveOptimizer;
	class Clique;

	// Value that corresponds to a non-assigned label
	const int INVALID_LABEL = -1;

	class Slave
	{
		public:
			/**
			* Slave constructor.
			* \param id Slave ID. This ID must be unique for every slave in a graph.
			* \param optimizer Optimizer used to optimize the slave.
			* \param nodesTotalAmount Total amount of nodes in the graph.
			* \param nodesApproxSlaveAmount Approximative amount of nodes in the slave. Even if a node is used by two cliques in the same slave, it counts just once (It is used to pre-allocate memory).
			* \param cliquesApproxSlaveAmount Approximative amount of cliques in the slave (It is used to pre-allocate memory).
			* \param labelTable Label table that is used to optimize the graph.
			*/
			Slave(int id, SlaveOptimizer * optimizer, int nodesTotalAmount, int nodesApproxSlaveAmount, int cliquesApproxSlaveAmount, LabelTable * labelTable);	
			
			~Slave();

			/**
			* Optimize this slave.
			*/
			float optimize();

			/**
			* Update the price that costs to choose a label to a node
			*/
			void updatePrice(int node, int label, float factor);

			/**
			* Adds a node to the slave's set of nodes.
			*/
			void addNode(int node) { 
				// Add the node to the array of nodes used in the slave
				nodes->append(node); 

				// Create its corresponding prices array for each label and initilizes it in 0
				float * prices = new float[labelTable->getLabelsCount()];
				nodesPrices->append(prices);
				std::fill(prices, prices + labelTable->getLabelsCount(), 0.0f);
				
				// Add this node to the localNodeId dictionary
				localNodeId[node] = nodes->size() - 1;
			}

			/**
			* Adds a clique to the clique's set of nodes.
			*/
			void addClique(Clique * clique) {cliques->append(clique); }

			/**
			* Returns a reference to a int* array containing the IDs of the nodes contained in the slave
			*/
			int * getNodes() { return nodes->data(); }

			/**
			* Returns a reference to a Clique* array containing the cliques contained in the slave
			*/
			Clique ** getCliques() { return cliques->data(); }
			
			/**
			* Returns a reference to the array that contains the label assigned to every node.
			* The vector index indicates the node number, and its value indicates the label. If this node
			* isn't included in the Slave, its value will be DD::INVALID_LABEL.
			*/
			int * getNodesLabelAssignements() { return nodesLabelAssignements; }

			/**
			* Returns the total amount of nodes in the DDGraph.
			*/
			int getNodesTotalAmount() { return nodesTotalAmount; }

			/**
			* Returns the amount of nodes in the slave.
			*/
			int getNodesSlaveAmount() { return nodes->size(); }
			
			/**
			* Returns the amount cliques in the slave.
			*/
			int getCliquesSlaveAmount() { return cliques->size(); }
			
			int getId() { return id; }

			LabelTable * getLabelTable() { return labelTable; }

			/**
			* Calculate the sum of prices that each node must pay for choosing the label.
			*/
			float calculateLabelPrices(int * labelAssignement)
			{
				float price = 0.0f;

				for (int i = 0, size = nodes->size(); i < size; ++i)
					price += nodesPrices->at(i)[ labelAssignement[ nodes->at(i) ] ];

				return price;
			}

			void showLabelAssignements() 
			{ 
//				std::cout << "Slave " << getId() << std::endl;
				for (int i = 0; i < nodesTotalAmount; ++i )
					std::cout << nodesLabelAssignements[i] << " ";

				std::cout << std::endl;
			}

			void showPrices() 
			{ 
				std::cout << "Slave: " << getId() << std::endl;
				for (int i = 0; i < getNodesSlaveAmount(); ++i )
				{
					std::cout << "-- Node Local: " << i << ", Global: " << nodes->at(i) << " -> ";
					for (int j = 0; j < getLabelTable()->getLabelsCount(); ++j)
					{
						std::cout << nodesPrices->at(i)[j] << "  ";
					}
					std::cout << std::endl;
				}
			}

			/**
			* Returns the prices array corresponding to the node "localIdNode". Note: the localIdNode is the Id that corresponds
			* to the node in the slave; it is, in fact, the position of this node in the slave nodes vector. If you want to use
			* the global id of the node, you can use: getPricesGlobalId( globalIdNode ).
			*/
			float * getPrices(int localIdNode) { return nodesPrices->at(localIdNode); }
			
			/**
			* Returns the prices array corresponding to the node "globalIdNode". Note: the globalIdNode is the Id that corresponds
			* to the node in the graph. If you want to use the local id of the node, you can use: getPrices( localIdNode )
			* This method is slower than "getPrices" because it has one more derefencing.
			*/
			float * getPricesGlobalId(int globalIdNode) { return nodesPrices->at( localNodeId[globalIdNode] ); }

			/**
			* It initializes the prices of every pair (node, label) with the value given in 'prices'. The size of
			* prices must be NumberOfLabels X NumberOfNodes, and to index it the following rule must be used:
			* index = nodeIndex + NumberOfNodes * labelIndex
			*/			
			void initializePrices(float * prices);
			
			/**
			* Reset all the prices to 0.
			*/		
			void resetPrices();
			
		private:
			
			int id;							///< Slave ID. This ID must be unique for every slave in a graph.
			
			int nodesTotalAmount;			///< Total amount of nodes in the graph.
			int nodesApproxSlaveAmount;		///< Approximative amount of nodes in the slave. Even if a node is used by two cliques in the same slave, it counts just once (It is used to pre-allocate memory).
			int cliquesApproxSlaveAmount;	///< Approximative amount of cliques in the slave (It is used to pre-allocate memory).

			int * nodesLabelAssignements;	///< Sparse array that contains the label values assigned to the nodes. 
			int * localNodeId;				///< This array stores the local name ID that corresponds to the node in the slave. It is used to improve the performance. Given a global index the array contains the local index useful to access to "nodes" and "nodesPrices" arrays.
			
			SlaveOptimizer * optimizer;		///< Optimizer used to optimize the slave.
			
			QVector<float *> * nodesPrices; ///< Array that stores the price of every label for every node. Those prices are used by DualDecomposition to achieve convergence. It has the same size than 'nodes' and its value corresponds to the node whose global index is at the same position in 'nodes'.
			QVector<int> * nodes;			///< Vector containing the nodes that are in the slave. 
			QVector<Clique *> * cliques;	///< Vector containing the cliques included in the slave. 

			LabelTable * labelTable;		///< Label table that will be used to optimize the slave

			//TODO DD: Destroy 'nodesAssignements', 'nodes' and 'cliques' arrays. 
			//TODO DD: Decide what should we do with the 'optimizer'. Save a copy and destroy it?
	};
};

#endif