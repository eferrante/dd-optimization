#include "DualDecomposition.h"

using namespace std;

namespace DD
{
	//==========================================================================================================

	DualDecomposition::DualDecomposition(DDGraph * graph, LabelTable * labelTable, int maxIterations, float alpha) :
		graph(graph), labelTable(labelTable), maxIterations(maxIterations), slaves(0), cliques(0), solutionLabeling(0), alpha(alpha),
		primalEstimatedSolution(0), bestPrimalEstimatedSolution(0), initialPrices(0)
	{

	}

	DualDecomposition::~DualDecomposition()
	{
		for (int i = 0; i < slaves->size(); ++i)
			delete slaves->at(i);
		delete slaves;
		
		for (int i = 0; i < cliques->size(); ++i)
			delete cliques->at(i);
		delete cliques;
		
		delete[] primalEstimatedSolution;
		delete[] bestPrimalEstimatedSolution;
		delete[] solutionLabeling;
	}

	//==========================================================================================================

	void DualDecomposition::applyPartitioners()
	{
		// Delete every existing slave
		if (slaves)
			for (int i = 0, size = slaves->size(); i < size; ++i)
				delete slaves->at(i);
		
		// Delete every existing clique
		if (cliques)
			for (int i = 0, size = slaves->size(); i < size; ++i)
				delete slaves->at(i);

		if (primalEstimatedSolution)
			delete [] primalEstimatedSolution;
		
		if (bestPrimalEstimatedSolution)
			delete [] bestPrimalEstimatedSolution;

		// Clean the graph cliques
		graph->eraseSlavesInformation();

		slaves = new QList<Slave *>;
		cliques = new QList<Clique *>;

		// Partition the graph in cliques and slaves using the partitioners
		for (int i = 0; i < partitioners.size(); ++i)
			partitioners.at(i)->partition(graph, labelTable, slaves, cliques);

		primalEstimatedSolution = new int[graph->getNodesCount()];
		bestPrimalEstimatedSolution = new int[graph->getNodesCount()];
	}
	
	//==========================================================================================================
	float DualDecomposition::getTotalEnergy()
	{
		return getTotalEnergy(this->solutionLabeling);
	}

	void DualDecomposition::saveBestPrimal()
	{
		for (int i = 0; i < graph->getNodesCount(); ++i)
			bestPrimalEstimatedSolution[i] = primalEstimatedSolution[i];
	}

	float DualDecomposition::evaluateTotalCostSumZeroLabeling()
	{
		int * labeling = new int[graph->getNodesCount()];
		std::fill(labeling, labeling+graph->getNodesCount(), 0);

		float energy = getTotalEnergy(labeling);

		delete [] labeling;

		return energy;
	};

	float DualDecomposition::getTotalEnergy(int * labeling)
	{
		float energy = 0.0f;

		if (cliques)
			for (int i = 0; i < cliques->size(); i++)
				energy += cliques->at(i)->evaluate( labeling );
		
		return energy;
	}

	//==========================================================================================================

	void DualDecomposition::reconstructFinalSolution() 
	{
		if (solutionLabeling)
			delete solutionLabeling;

		solutionLabeling = new int[graph->getNodesCount()];

		//int * labelAssignments;

		//if (slaves)
		//	for (int i = 0; i < slaves->size(); ++i)
		//	{
		//		labelAssignments = slaves->at(i)->getNodesLabelAssignements();

		//		for (int j = 0; j < graph->getNodesCount(); ++j)
		//			if (labelAssignments[j] != INVALID_LABEL)
		//				solutionLabeling[j] = labelAssignments[j];

		//	}

		for (int i = 0; i < graph->getNodesCount(); ++i)
			solutionLabeling[i] = bestPrimalEstimatedSolution[i];
	}


	//==========================================================================================================

	float DualDecomposition::run()
	{
		int it = 1;
		int maxSemiConvergenceIt = 2000;
		float bestPrimal = std::numeric_limits<float>::max();

		if (graph && slaves && cliques)
		{
			bool convergence = false;
			bool semiConvergence = false;

			// write info file
			//std::ofstream dualEnergyFile;	

			//dualEnergyFile.open( "dualEnergy.csv" );
			cout << "Alpha: " << alpha << endl;
	
			float factor = alpha;

            float dualEnergy;
			
			while (!convergence && (it < maxIterations))// && (!semiConvergence || !(it > maxSemiConvergenceIt)) )
			{			
                dualEnergy = 0;
                std::cout << "Iteration " << it <<std::endl;

				int size = slaves->size();
				// Optimize every slave
				#pragma omp parallel for
				for (int i = 0; i < size; ++i)
				{
					dualEnergy += slaves->at(i)->optimize();;
					//slaves->at(i)->showLabelAssignements();
					//std::cout << "Energy: " << e << std::endl;
					//slaves->at(i)->showPrices();
				}

				// Update the prices of the slaves and check the convergence of the algorithm at the same time
				convergence = updatePrices(it, factor, semiConvergence);
				//std::cout << "   Dual Energy: " << dualEnergy << std::endl;
				//dualEnergyFile << it << ", " << dualEnergy << std::endl;
				
				float primalEnergy = getTotalEnergy(primalEstimatedSolution);
				
				// If there are some initial prices, they are summed to the primal energy cause they are considered as Unary Potentials.
				if (initialPrices)
					primalEnergy += calculatePrimalSolutionInitialPrices();

				if (primalEnergy < bestPrimal)
				{
					bestPrimal = primalEnergy;
					saveBestPrimal();
				}

				if ((it %200) == 0) {
				std::cout << "   It: " << it << std::endl;
				std::cout << "   Semi convergence: " << semiConvergence << std::endl;
				std::cout << "   Best Primal Energy: " << bestPrimal << std::endl;
				std::cout << "   Current Primal Energy: " << primalEnergy << std::endl;
				std::cout << "   Dual Energy: " << dualEnergy << std::endl;
				std::cout << "   Primal Dual Gap: " << bestPrimal - dualEnergy << std::endl <<std::endl;

				}
				if ((bestPrimal - dualEnergy) < alpha)
                    factor  = fabs(bestPrimal - dualEnergy) / graph->getNodesCount();
				else
                {
					factor = alpha / static_cast<float>(it);
                    if (factor == 0.0f) factor = 0.01f;
                }

                if ((bestPrimal - dualEnergy) < 1e-6)
                    convergence = true;

				it++;
			}

			if (convergence)
				cout << "Convergence: True in " << it << " iterations" << endl;
			else
				cout << "Convergence: False" << endl;

			if (semiConvergence)
				cout << "Semi Convergence: True in " << it << " iterations" << endl;
			else
				cout << "Semi Convergence: False" << endl;

			reconstructFinalSolution();

            cout<<"Primal energy: "<<bestPrimal<<" dual energy: "<<dualEnergy<<endl;

			//dualEnergyFile.close();

			return getTotalEnergy();
		}
		else
		{
			std::cout << "DualDecomposition ERROR: Not graph or slaves or cliques defined";
			return -1;
		}
	}

	//==========================================================================================================

	bool DualDecomposition::updatePrices(int it, float energyGap, bool & semiConvergence)
	{
		int nodesCount = graph->getNodesCount();

		int slavesCount = 0;
		float factor = energyGap;

		Slave ** nodeSlaves;
		int notAgreeing = 0;
		int nodesNotAgreeing=0;
		int labelsCount = labelTable->getLabelsCount();

		int * labelsCounting = new int[labelsCount];
		if ((it %200) == 0)
			std::cout << "   Factor: " << factor << std::endl << std::endl;
		
		bool converge = true;
		semiConvergence = true;

		// The semi convergence condition is that all the nodes agree at least in a specified percentage of the nodes	
		float semiConvergenceRatio = 0.5f;

		// For each node n
		for (int n = 0; n < nodesCount; ++n)
		{
			bool agreeing = true;

			std::fill(labelsCounting, labelsCounting + labelsCount, 0);

			// For each pair of slaves s1, s2 where the node n is used
			graph->getSlaves(n, nodeSlaves, slavesCount);
			
			//std::cout << "Alpha " << (alpha / it) / slavesCount << endl;
			int l1, l2;
			
			l1 = (nodeSlaves[0]->getNodesLabelAssignements())[n];
			
			for (int s1 = 0; s1 < slavesCount; ++s1)
				for (int s2 = s1 + 1; s2 < slavesCount; ++s2)
				{
					// Get the label that corresponds to node p in each slave
					l1 = (nodeSlaves[s1]->getNodesLabelAssignements())[n];
					l2 = (nodeSlaves[s2]->getNodesLabelAssignements())[n];

					// If the assigned label is not the same
					if ( l1 != l2 ) 
					{
						// Update the prices for the labels l1, l2 in the slaves s1, s2
						//factor = (alpha / it);// / slavesCount;
						//if (it < 200)
						//factor = (alpha / (( (static_cast<float>(it)) * static_cast<float>(slavesCount))));
						factor = energyGap / static_cast<float>(slavesCount);

						//factor = alpha;
						nodeSlaves[s1]->updatePrice(n, l1, factor);
						nodeSlaves[s1]->updatePrice(n, l2, -factor);
						nodeSlaves[s2]->updatePrice(n, l1, -factor);
						nodeSlaves[s2]->updatePrice(n, l2, factor);
						converge = false;
						notAgreeing++;
						agreeing = false;
					}	
				}
			
			if(agreeing) 
			{
				primalEstimatedSolution[n] = l1;
			}
			else
			{
				nodesNotAgreeing++;

				// Compute the primal label using a voting mechanism

				//Count the amount of times that every label appears
				for (int s = 0; s < slavesCount; ++s)
					labelsCounting[(nodeSlaves[s]->getNodesLabelAssignements())[n]]++;
				
				int maxAmount = -1;
				int maxLabel;

				// Choose the label that appears max times
				for (int s = 0; s < slavesCount; ++s)
					if (labelsCounting[(nodeSlaves[s]->getNodesLabelAssignements())[n]] > maxAmount)
					{
						maxLabel = (nodeSlaves[s]->getNodesLabelAssignements())[n];
						maxAmount = labelsCounting[(nodeSlaves[s]->getNodesLabelAssignements())[n]];
					}

				// Assigns the label to the primalEstimatedSolution
				primalEstimatedSolution[n] = maxLabel;
				float ratio = static_cast<float>(maxAmount) / static_cast<float>(slavesCount);
				// Check for the semi convergence criteria
				if (ratio < semiConvergenceRatio) 
					semiConvergence = false;

				//if (it % 200 == 0)
				//	cout << "Converging ratio node [" << n << "]: " << ratio << endl;
			}
		}

//        if (it % 2 == 0)
		{
			cout << "Pairs not agreeing: " << notAgreeing << endl;
			cout << "Nodes not agreeing: " << nodesNotAgreeing << endl;
		}
		
		delete [] labelsCounting;

		return converge;
	}

	void DualDecomposition::show() 
	{
		graph->show();
	
		if (slaves)
		{
			cout << "[ == Slaves == ]" << endl;
			for (int i = 0; i < slaves->size(); ++i)
			{
				Slave * slave = slaves->at(i);
				cout << "|- Slave " << slave->getId() << endl;
				cout << "  |- Nodes" << endl;

				for (int j = 0; j < slave->getNodesSlaveAmount(); ++j)
				{
					cout << "    |- " << slave->getNodes()[j] << endl;
				}
				
				cout << "  |- Cliques" << endl;

				for (int j = 0; j < slave->getCliquesSlaveAmount(); ++j)
				{
					Clique * clique = slave->getCliques()[j];
					cout << "    |- (";
					for (int k = 0; k < clique->getOrder(); ++k)
						cout << clique->getNodes()[k] << " ";
					cout << ")" << endl;
				}
				cin.get();			
			}
		}
	}

	void DualDecomposition::initializePrices(float * prices)
	{
		int size = slaves->size();
		
		initialPrices = prices;

		#pragma omp parallel for
		for (int i = 0; i < size; ++i)
			slaves->at(i)->initializePrices(prices);
			
	}

	void DualDecomposition::resetSlavesPrices() 
	{
		int size = slaves->size();

		#pragma omp parallel for
		for (int i = 0; i < size; ++i)
			slaves->at(i)->resetPrices();
	}

	// It calculates the extra label prices for the primal when some initialPrices are set.
	// To do that, it just add the prices corresponding to current labeling in the initialPrices.
	float DualDecomposition::calculatePrimalSolutionInitialPrices()
	{
		float price = 0.0f;
		if (initialPrices)
			for (int i = 0, nodesCount = graph->getNodesCount(); i < nodesCount; ++i)
			{
				price += initialPrices[i + nodesCount * primalEstimatedSolution[i] ] * graph->getSlavesCount(i); 		
			}
		return price;
	}

}
