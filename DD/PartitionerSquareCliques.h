#ifndef PARTITIONERSQUARECLIQUES_H
#define PARTITIONERSQUARECLIQUES_H

#include "DDGraph.h"
#include "Slave.h"
#include "Clique.h"
#include "Partitioner.h"
#include "SlaveOptimizer.h"
#include "DDCostFunction.h"
#include "LabelTable.h"

#include <QList>

namespace DD {

    /**
     * \brief PartitionerSquares class. It partitions a graph in
     * regular squares that cover the complete grid. Each square is a slave.
     *
     * \author Enzo Ferrante.
     * \date 3-12-2012
     *
     */

    class PartitionerSquareCliques : public Partitioner
    {

        public:
            /**
            * Squared partitioner constructor.
            */
            PartitionerSquareCliques(SlaveOptimizer * optimizer, DDCostFunction * costFunction) :
                Partitioner(optimizer, costFunction) {}


            /**
            * This method partitiones the graph in different slaves and cliques.
            */
            void partition(DDGraph * graph, LabelTable * labelTable, QList<Slave*> * slaves, QList<Clique*> * cliques);
    };
};



#endif // PARTITIONERSQUARECLIQUES_H
