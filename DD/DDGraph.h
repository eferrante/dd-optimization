#ifndef DDGRAPH_DD_H
#define DDGRAPH_DD_H

#include <QVarLengthArray>

#include "Slave.h"

namespace DD
{
	/**
     * \brief DDGraph class. It models a graph that will be optimized using Dual Decomposition algorithm.
	 * 
	 * \author Enzo Ferrante.
	 * \date 28-11-2012
	 *
	 */
	static const int MAX_SLAVES = 10;

	class DDGraph
	{
		public:
            DDGraph(int rows, int cols, int dimLabel);

			~DDGraph();

			/**
			* This methods shows the graph
			*/
			virtual void show() {};

			/**
			* Returns the dimension of the labels used in the graph.
			*/
			int getDimLabel() { return dimLabel; }

			/**
			* Returns the amount of nodes in the graph.
			*/
			int getNodesCount() { return nodesCount; };

			/**
			* It erases the slaves associated to every node.
			*/
			void eraseSlavesInformation();

			/**
			* It assigns to the given parameters the slaves array and the amount of slaves where the node "vertexNode" is included.
			*/
			void getSlaves(int node, Slave ** & slaves, int & slavesCount) {
				slaves = nodesSlaves[node].data();
				slavesCount = nodesSlaves[node].size();
			}
			
			/**
			* It returns the amount of slaves where the node is involved in the graph.
			*/
			int getSlavesCount(int node) {
				return nodesSlaves[node].size();
			}

			void addSlave(const int & node, Slave * s) {
				nodesSlaves[node].append(s);
			}
	
            int getCols() { return numCols; }
            int getRows() { return numRows; }

		protected:

			QVarLengthArray<Slave *, MAX_SLAVES> * nodesSlaves;     ///> Vector that stores, for each node, the slaves where it is used.

			int nodesCount;											///> Total amount of nodes in the graph

			int dimLabel;											///> Dimension of the node labels

            int numCols, numRows;

            //TODO DD: Destroy the array of "nodesSlaves".
	};
};

#endif
