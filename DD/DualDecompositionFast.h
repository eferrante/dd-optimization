#ifndef DUALDECOMPOSITIONFAST_DD_H
#define DUALDECOMPOSITIONFAST_DD_H

#include "DDGraph.h"
#include "Slave.h"
#include "Clique.h"
#include "LabelTable.h"
#include "Partitioner.h"
#include <limits>

#include <QList>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sstream>
#include <fstream>

namespace DD
{
	/**
	 * \brief Dual Decomposition main algorithm. It takes a DDGraph, partitiones it using the given Partitioner
	 * and optimizes it.
	 * 
	 * 
	 * \author Enzo Ferrante.
	 * \date 28-11-2012
	 *
	 */
	
	class DualDecompositionFast
	{
		public:

			/**
			* DualDecompositionFast constructor. 
			* \param graph Graph that will be optimized with the current values of its nodes
			* \param partitioner Partitioner used to decompose the graph on slaves and cliques
			* \param labelTable table including ossible labels that can be assigned to the graph
			* \param maxIterations maximum of iterations that can be computed to achieve convergence
			*/
			DualDecompositionFast(DDGraph * graph, LabelTable * labelTable, int maxIterations, float alpha);

			~DualDecompositionFast();

			/**
			* Adds a partitioner that will be applied to the graph before optimizing it.
			*/
			void addPartitioner(Partitioner * p)
			{
				partitioners.append(p);
			}

			/**
			* Partitionate the graph. This method must be executed when the graph structure is modified,
			* in order to re-partition it.
			*/
			void applyPartitioners();

			/**
			* This method executes the optimization algorithm.
			*/
			float run();

			/**
			* This method shows the decomposed problem (slaves and cliques)
			*/
			void show();
			
			void showSolution() {
				std::cout << "Solution: " << std::endl;
				for (int i = 0; i < graph->getNodesCount(); ++i )
					std::cout << solutionLabeling[i] << " ";
				std::cout << std::endl;
			}

			int * getLabeling() { return solutionLabeling; }

			void getLabeling(int * labeling)
			{
				for(int p = 0; p < graph->getNodesCount(); ++p)
					labeling[p] = solutionLabeling[p];
			}
		
			QList<Clique *> * getCliques() {return cliques;}

			float evaluateTotalCostSumZeroLabeling();

			/**
			* It initializes the prices of every pair (node, label) with the value given in 'prices'. The size of
			* prices must be NumberOfLabels X NumberOfNodes, and to index it the following rule must be used:
			* index = nodeIndex + NumberOfNodes * labelIndex
			*/	
			void initializePrices(float * prices);
			
			/**
			* This method calculates the final energy of the graph as a sum of the energy of each clique
			*/
			float getTotalEnergy();

			/**
			* Set all the slave prices to 0.
			*/
			void resetSlavesPrices();

		private:
			float getTotalEnergy(int * labeling);

			/**
			* This method reconstruct the final labeling solution combining the slaves solutions
			*/
			void reconstructFinalSolution();

			/**
			* This method updates the prices of the slaves and check the convergence of the algorithm at the same time
			*/
			bool updatePrices(int it, float energyGap);

			QList<Slave *> * slaves;		///> Slaves used to divide the graph
			QList<Clique *> * cliques;		///> Cliques of the graph

			DDGraph * graph;				///> Graph to optimize
			LabelTable * labelTable;		///> Table including the possible labels

			int maxIterations;				///> Maximum of iterations that can be computed to achieve convergence

			float alpha;					///> Factor that adjust the price updating
			
			QList<Partitioner *> partitioners;	///> List of partitioners that will be applied to the graph. Those are applied in the same order that they are stored in this list.

			int * solutionLabeling;				///> Final solution

			float * initialPrices;

			//TODO DD: Destroy the lists (and every element) of slaves and cliques.
	};

};

#endif