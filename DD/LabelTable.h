#ifndef LABELTABLE_DD_H
#define LABELTABLE_DD_H

namespace DD
{
	/**
	 * \brief (Abstract class) A table containing the possible labels that DualDecomposition is going to use.
	 * 
	 * \author Enzo Ferrante.
	 * \date 28-11-2012
	 *
	 */
	class LabelTable
	{
		public:	
			LabelTable(int dimLabel, int labelsCount) : dimLabel(dimLabel), labelsCount(labelsCount) {}
			
			/**
			* Returns the label corresponding to the given index.
			*/
            virtual const float * getLabel(int labelIndex) = 0;

			// Getters 
			int getDimLabel() { return dimLabel; };
			int getLabelsCount() { return labelsCount; };
			
		protected:	
			int dimLabel;				///< Dimension of each label		
			int labelsCount;			///< Amount of labels in the table
	};
};

#endif
